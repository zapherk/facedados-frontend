import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AlertComponent } from './components/alert/alert.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';

import {MatDatepickerModule} from '@angular/material/datepicker'; 
import {
  MatSelectModule,
  MatOptionModule,
  MatListModule,
  MatTabsModule,
  MatIconModule,
  MatMenuModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatButtonModule,
} from '@angular/material';
import { FilmesComponent } from './components/filmes/filmes.component';
import { MusicasComponent } from './components/musicas/musicas.component';
import { DatePipe } from '@angular/common';

import {TableModule} from 'primeng/table';

@NgModule({
  declarations: [
    MusicasComponent,
    FilmesComponent,
    AlertComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    AppComponent
  ],
  imports: [
    MatSelectModule,
    MatOptionModule,
    MatListModule,
    MatTabsModule,
    TableModule,
    MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatCardModule,
    MatFormFieldModule,
    MatButtonModule,
    FlexLayoutModule,
    MatInputModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
