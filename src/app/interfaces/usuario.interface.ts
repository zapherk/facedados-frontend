/**
 * Interface para usuário.
 *
 * **_Abreviação_: `iUsr`**
 */
export interface IUsuario {

  /**
   * Login do Usuário
   */
  login: string;

  /**
   * Nome do Usuário.
   */
  name: string;

  /**
   * Cidade natal do Usuário.
   */
  birthtown: string;

  /**
   * Data de nascimento do Usuário.
   */
  birthdate: string;

  /**
   * Senha do Usuário.
   */
  password: string;
  
}
