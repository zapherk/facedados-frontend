/**
 * Interface para curte artista musical.
 *
 */
export interface IMusica {

    /**
     * Login do Usuário
     */
    usr: string;
  
    /**
     * URI da banda.
     */
    banda_uri: string;
  
    /**
     * Nome da banda.
     */
    banda_nome: string;
  
    /**
     * Nota da banda.
     */
    nota: string;
    
  }