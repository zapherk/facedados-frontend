/**
 * Interface para o get de usuário.
 *
 * **_Abreviação_: `iUsrGet`**
 */
export interface IUsuarioGet {

    /**
     * Id gerado pelo banco (usaremos como um fake token)
     */
    _id: any;

    /**
     * Login do Usuário
     */
    login: string;
  
    /**
     * Nome do Usuário.
     */
    nome_completo: string;
  
    /**
     * Cidade natal do Usuário.
     */
    cidade_natal: string;

    /**
     * Data de nascimento do Usuário.
     */
    data_nascimento: any;
  
  }
  