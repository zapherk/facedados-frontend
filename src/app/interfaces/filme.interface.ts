/**
 * Interface para curte filme.
 *
 */
export interface IFilme {

  /**
   * Login do Usuário
   */
  usr: string;

  /**
   * URI do filme.
   */
  filme_uri: string;

  /**
   * Nome do filme.
   */
  filme_nome: string;

  /**
   * Nota do filme.
   */
  nota: string;
  
}
  