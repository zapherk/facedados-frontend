import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  Validators,
  FormControl
} from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService } from '../../services/alert.service';
import { AuthenticationService } from '../../services/authentication.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Usuario } from 'src/app/models/usuario';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
  returnUrl: string;
  public username = new FormControl('', [Validators.required]);
  public password = new FormControl('', [Validators.required]);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private usuarioService: UsuarioService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  public login(): void {
    this.usuarioService.login(this.username.value, this.password.value).pipe(first()).subscribe(
      response => {
        this.usuarioService.getUsuario(response.body).pipe(first()).subscribe(
          (usuario: Usuario) => {            
            this.authenticationService.login(usuario);
            this.router.navigate([this.returnUrl]);
          },
          () => {
            this.alertService.error("Não foi possível encontrar o Usuário.")
          }
        )
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public register(): void {
    this.router.navigate(['registrar']);
  }

}
