import { Component, OnInit } from '@angular/core';

import { UsuarioService } from '../../services/usuario.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Usuario } from 'src/app/models/usuario';
import { AlertService } from 'src/app/services/alert.service';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit {
  currentUser: Usuario;
  usuarios: Usuario[] = Array<Usuario>();
  amigos: Usuario[] = Array<Usuario>();
  sugeridos: Usuario[] = Array<Usuario>();

  constructor(
    private authenticationService: AuthenticationService,
    private usuarioService: UsuarioService,
    private alertService: AlertService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  public globalFilterFields = [
    (usr: Usuario) => usr.name,
    (usr: Usuario) => usr.hometown,
    (usr: Usuario) => usr.login
  ];


  ngOnInit() {
    this.usuarioService.getUsrs().pipe().subscribe(
      (usuarios: Usuario[]) => {
        this.usuarios = usuarios;
      }
    );
    this.usuarioService.getAmigos(this.currentUser.login).pipe().subscribe(
      (amigos: Usuario[]) => {
        this.amigos = amigos;
      }
    );
    this.usuarioService.getAmigosSug(this.currentUser.login).pipe().subscribe(
      (sugeridos: Usuario[]) => {
        this.sugeridos = sugeridos;
      }
    );

  }

  public adicionar(usr2: string): void {
    this.usuarioService.adicionarAmigo(this.currentUser.login, usr2).pipe().subscribe(
      () => {
        this.alertService.success('Adicionado com sucesso!');
      },
      error => {
        this.alertService.error(error);
      }
    )
  }

}
