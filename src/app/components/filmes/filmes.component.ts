import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../services/authentication.service';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { FormControl } from '@angular/forms';

@Component({ templateUrl: 'filmes.component.html' })
export class FilmesComponent implements OnInit {
  currentUser: Usuario;
  filmes = Array<string>();
  sugestoes = Array<string>();
  public nome = new FormControl('');
  public uri = new FormControl('');
  public nota = new FormControl('');

  constructor(
    private authenticationService: AuthenticationService,
    private usuarioService: UsuarioService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
    this.usuarioService.getFilmes(this.currentUser.login).pipe().subscribe(
      (filmes: string[]) => {
        this.filmes = filmes;
      }
    )
    this.usuarioService.getFilmesSug(this.currentUser.login).pipe().subscribe(
      (sugestoes: string[]) => {
        this.sugestoes = sugestoes;
      }
    )
  }

  public adicionar(): void {
    this.usuarioService.curtirFilme(this.currentUser.login, this.uri.value, this.nome.value, this.nota.value).pipe().subscribe()
  }

}
