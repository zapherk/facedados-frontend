import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../services/authentication.service';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { FormControl } from '@angular/forms';

@Component({ templateUrl: 'musicas.component.html' })
export class MusicasComponent implements OnInit {
  currentUser: Usuario;
  musicas = Array<string>();
  sugestoes = Array<string>();
  public nome = new FormControl('');
  public uri = new FormControl('');
  public nota = new FormControl('');
  
  constructor(
    private authenticationService: AuthenticationService,
    private usuarioService: UsuarioService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
    this.usuarioService.getMusicas(this.currentUser.login).pipe().subscribe(
      (musicas: string[]) => {
        this.musicas = musicas;
      }
    )
    this.usuarioService.getMusicasSug(this.currentUser.login).pipe().subscribe(
      (sugestoes: string[]) => {
        this.sugestoes = sugestoes;
      }
    )
  }
  
  public adicionar(): void {
    this.usuarioService.curtirMusica(this.currentUser.login, this.uri.value, this.nome.value, this.nota.value).pipe().subscribe()
  }
}
