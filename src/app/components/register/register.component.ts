import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService } from '../../services/alert.service';
import { UsuarioService } from '../../services/usuario.service';
import { AuthenticationService } from '../../services/authentication.service';


@Component({ templateUrl: 'register.component.html' })
export class RegisterComponent implements OnInit {
  loading = false;
  submitted = false;

  public registerForm = new FormGroup({
    login: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    name: new FormControl('', [Validators.required]),
    hometown: new FormControl('', [Validators.required]),
    birthdate: new FormControl(new Date(), [Validators.required])
  });

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private usuarioService: UsuarioService,
    private alertService: AlertService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {    
  }


  public register(): void {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.usuarioService.register(this.registerForm.value).pipe(first()).subscribe(
      data => {
        this.alertService.success('Registrado com sucesso!', true);
        this.router.navigate(['/login']);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }

  public cancel(): void {
    this.router.navigate(['/']);
  }
}
