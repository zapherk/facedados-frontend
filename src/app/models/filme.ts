/**
 * Classe de Filme.
 * 
 * **_Abreviação_: `flm`**
 */
export class Filme {
  private _nome: string;

  /**
   * @param nome Nome do Filme.
   */
  constructor(
    nome: string
  ) {
    this.nome = nome;
  }

  public get nome(): string {
    return this._nome;
  }
  
  public set nome(value: string) {
    this._nome = value;
  }
}