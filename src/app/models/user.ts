export class User {
    public login: string;
    public name: string;
    public hometown: string;
    public birthdate: Date;
    public token: string;

    constructor(
        login: string,
        name: string,
        hometown: string,
        birthdate: Date,
        token: string
    ) {
        this.login = login;
        this.name = name;
        this.hometown = hometown;
        this.birthdate = birthdate;
        this.token = token;
    }
}