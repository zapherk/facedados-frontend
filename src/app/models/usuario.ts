/**
 * Classe de Usuário.
 * 
 * **_Abreviação_: `usr`**
 */
export class Usuario {
  public id?: number;
  private _login: string;
  private _name: string;
  private _hometown: string;
  private _birthdate: Date;
  private _password?: string;
  public token?: string;

  /**
   * @param login Login do usuário para acesso do sistema.
   * @param name Nome do usuário.
   * @param hometown Cidade natal do usuário.
   * @param birthdate Data de nascimento do usuário.
   */
  constructor(
    login: string,
    name: string,
    hometown: string,
    birthdate: Date,
    token?: string
  ) {
    this.login = login;
    this.name = name;
    this.hometown = hometown;
    this.birthdate = birthdate;
    this.token = token;
  }

  public get login(): string {
    return this._login;
  }

  public set login(value: string) {
    this._login = value;
  }

  public get name(): string {
    return this._name;
  }

  public set name(value: string) {
    this._name = value;
  }

  public get hometown(): string {
    return this._hometown;
  }

  public set hometown(value: string) {
    this._hometown = value;
  }

  public get birthdate(): Date {
    return this._birthdate;
  }

  public set birthdate(value: Date) {
    this._birthdate = value;
  }

  public get password(): string {
    return this._password;
  }

  public set password(value: string) {
    this._password = value;
  }

}