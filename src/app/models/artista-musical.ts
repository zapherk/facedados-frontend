/**
 * Classe de Artista Musical.
 * 
 * **_Abreviação_: `artMsc`**
 */
export class ArtistaMusical {
  private _nome: string;

  /**
   * @param nome Nome do artista musical.
   */
  constructor(
    nome: string
  ) {
    this.nome = nome;
  }

  public get nome(): string {
    return this._nome;
  }
  
  public set nome(value: string) {
    this._nome = value;
  }
}
