import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Usuario } from '../models/usuario';
import { IUsuario } from '../interfaces/usuario.interface';
import { Observable, of, throwError } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { IUsuarioGet } from '../interfaces/usuarioGet.interface';
import { IFilme } from '../interfaces/filme.interface';
import { IMusica } from '../interfaces/musica.interface';

@Injectable({ providedIn: 'root' })
export class UsuarioService {
  constructor(
    private http: HttpClient,
    private datePipe: DatePipe
    ) { }

  baseUrl = 'http://127.0.0.1:5000'

  /**
   * Método que retorna observable de undefined quando registra um usuário no banco.
   * 
   * @param usuario Usuário a ser registrado no banco de dados.
   */
  public register(usuario: Usuario): Observable<undefined> {
    const reqBody: IUsuario = {
      login: usuario.login,
      name: usuario.name,
      birthtown: usuario.hometown,
      birthdate: usuario.birthdate
      ? this.datePipe.transform(usuario.birthdate, 'yyyy-MM-dd')
      : '',
      password: usuario.password
    }
    return this.http.post<HttpResponse<any>>(
      `${this.baseUrl}/registrar`,
      reqBody,
      { observe: 'response' }
    ).pipe(
      mergeMap((res: HttpResponse<any>) => {
        return [200, 204].includes(res.status) ? of(undefined) : throwError('Erro ao registrar usuário.');
      })
    );
  }

  /**
   * Método que retorna observable de undefined quando o login é efetuado com sucesso.
   * 
   * @param usuario login do usuário.
   * @param senha senha do usuário.
   */
  public login(usuario: string, senha: string): Observable<any> {
    const reqBody: any = {
      login: usuario,
      password: senha
    }
    return this.http.post<HttpResponse<any>>(
      `${this.baseUrl}/login`,
      reqBody,
      { observe: 'response' }
    ).pipe(
      map( res => {
        return res
      })
    )
  }

  /**
   * Função que retorna os dados de um usuário a partir do seu login.
   * 
   * @param login login do usuário para buscar seus dados no banco.
   */
  public getUsuario(login: string): Observable<Usuario> {
    return this.http.get<IUsuarioGet>(
      `${this.baseUrl}/user/` + login
    ).pipe(
      map((iUsr: IUsuarioGet) => {
        return new Usuario(
          iUsr.login,
          iUsr.nome_completo,
          iUsr.cidade_natal,
          iUsr.data_nascimento.$date,
          iUsr._id.$oid
        );
      })
    );
  }

  /**
   * Método que retornar um observable de array de Usuários com todos os usuários do banco.
   */
  public getUsrs(): Observable<Usuario[]> {
    return this.http.get<IUsuarioGet[]>(
      `${this.baseUrl}/usuarios`
    ).pipe(
      map((iUsrs: IUsuarioGet[]) => {
        if (!iUsrs) {
          return new Array<Usuario>();
        }
        return iUsrs.map((iUsr: IUsuarioGet) => {
          return new Usuario(
            iUsr.login,
            iUsr.nome_completo,
            iUsr.cidade_natal,
            new Date(iUsr.data_nascimento) || new Date()
          );
        });
      })
    );
  }

  /**
   * Método que retornar um observable de array de Usuários com todos os usuários amigos do usuário logado.
   */
  public getAmigos(login: string): Observable<Usuario[]> {
    return this.http.get<IUsuarioGet[]>(
      `${this.baseUrl}/amigos/` + login
    ).pipe(
      map((iUsrs: IUsuarioGet[]) => {
        if (!iUsrs) {
          return new Array<Usuario>();
        }
        return iUsrs.map((iUsr: IUsuarioGet) => {
          return new Usuario(
            iUsr.login,
            iUsr.nome_completo,
            iUsr.cidade_natal,
            new Date(iUsr.data_nascimento) || new Date()
          );
        });
      })
    );
  }

  /**
   * Função que registra o usuário logado como amigo de outro usuário. 
   * 
   * @param usr1 Usuário logado.
   * @param usr2 Usuário a ser adicionado.
   */
  public adicionarAmigo(usr1: string, usr2: string): Observable<undefined> {
    const reqBody: any = {
      usr1: usr1,
      usr2: usr2
    };

    return this.http.post<HttpResponse<any>>(
      `${this.baseUrl}/adicionar`,
      reqBody,
      { observe: 'response' }
    ).pipe(
      mergeMap((res: HttpResponse<any>) => {
        return [200, 204].includes(res.status) ? of(undefined) : throwError('Erro ao adicionar amigo.');
      })
    );
  }

  /**
   * Get dos filmes curtidos pelo usuário.
   * 
   * @param login login do usuário logado.
   */
  public getFilmes(login: string): Observable<string[]> {
    return this.http.get<string[]>(
      `${this.baseUrl}/filmes/` + login
    ).pipe(
      map((filmes: string[]) => {
        return filmes;
      })
    );
  }

  /**
   * Get dos filmes sugeridos ao usuário.
   * 
   * @param login login do usuário logado.
   */
  public getFilmesSug(login: string): Observable<string[]> {
    return this.http.get<string[]>(
      `${this.baseUrl}/sugestao_filme/` + login
    ).pipe(
      map((filmes: string[]) => {
        return filmes;
      })
    );
  }

  /**
   * Get dos artistas musicais curtidos pelo usuário.
   * 
   * @param login login do usuário logado.
   */
  public getMusicas(login: string): Observable<string[]> {
    return this.http.get<string[]>(
      `${this.baseUrl}/artistas_musicais/` + login
    ).pipe(
      map((musicas: string[]) => {
        return musicas;
      })
    );
  }

  /**
   * Get dos artistas musicais sugeridos ao usuário.
   * 
   * @param login login do usuário logado.
   */
  public getMusicasSug(login: string): Observable<string[]> {
    return this.http.get<string[]>(
      `${this.baseUrl}/sugestao_artista/` + login
    ).pipe(
      map((musicas: string[]) => {
        return musicas;
      })
    );
  }

  /**
   * Get das sugestões de amigo.
   * 
   * @param login login do usuário logado.
   */
  public getAmigosSug(login: string): Observable<Usuario[]> {
    return this.http.get<IUsuarioGet[]>(
      `${this.baseUrl}/sugestao_amigo/` + login
    ).pipe(
      map((iUsrs: IUsuarioGet[]) => {
        if (!iUsrs) {
          return new Array<Usuario>();
        }
        return iUsrs.map((iUsr: IUsuarioGet) => {
          return new Usuario(
            iUsr.login,
            iUsr.nome_completo,
            iUsr.cidade_natal,
            new Date(iUsr.data_nascimento) || new Date()
          );
        });
      })
    );
  }

  /**
   * Função de curtir filme.
   * 
   * @param login login do usuário.
   * @param uri URI do filme.
   * @param nome nome do filme.
   * @param nota nota do filme.
   */
  public curtirFilme(login: string, uri: string, nome: string, nota: string): Observable<undefined> {
    const reqBody: IFilme = {
      usr: login,
      filme_uri: uri,
      filme_nome: nome,
      nota: nota
    }

    return this.http.post<IFilme>(
      `${this.baseUrl}/curtir_filme`,
      reqBody,
      { observe: 'response' }
    ).pipe(
      mergeMap((res: HttpResponse<any>) => {
        return [200, 204].includes(res.status) ? of(undefined) : throwError('Erro ao curtir filme.');
      })
    );
  }

  /**
   * Função de curtir música.
   * 
   * @param login login do usuário.
   * @param uri URI da música.
   * @param nome nome da música.
   * @param nota nota da música.
   */
  public curtirMusica(login: string, uri: string, nome: string, nota: string): Observable<undefined> {
    const reqBody: IMusica = {
      usr: login,
      banda_uri: uri,
      banda_nome: nome,
      nota: nota
    }

    return this.http.post<IMusica>(
      `${this.baseUrl}/curtir_artista`,
      reqBody,
      { observe: 'response' }
    ).pipe(
      mergeMap((res: HttpResponse<any>) => {
        return [200, 204].includes(res.status) ? of(undefined) : throwError('Erro ao curtir música.');
      })
    );
  }

}
