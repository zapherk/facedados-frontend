import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Usuario } from '../models/usuario';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<Usuario>;
  public currentUser: Observable<Usuario>;
  

  constructor() {
    this.currentUserSubject = new BehaviorSubject<Usuario>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Usuario {
    return this.currentUserSubject.value;
  }

  public login(usuario: Usuario): void {
    const user = new User(
      usuario.login,
      usuario.name,
      usuario.hometown,
      usuario.birthdate,
      usuario.token
    )

    localStorage.setItem('currentUser', JSON.stringify(user));
    this.currentUserSubject.next(usuario);    
  }

  public logout(): void {
    // remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
